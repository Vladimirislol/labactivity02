package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
public class Greeter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Please input an integer: ");
        int userInput = sc.nextInt();
        System.out.println(Utilities.doubleMe(userInput));
    }
}